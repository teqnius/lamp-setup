
    #====================================================#
    # Create Root CA in WSL Apache Folder (/etc/apache2) #
    #====================================================#

    #https://www.computertechblog.com/import-a-certificate-to-trusted-root-certification-authorities-using-command-prompt/
    #https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/
    #https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-18-04
    #https://www.dionysopoulos.me/forge-your-own-ssl-certificates-for-local-development.html

    cert_path_wsl='/etc/apache2/ssl_cert'
    cert_path_win='C:\local_dev_certs'
    cert_path_win_wsl='/mnt/c/local_dev_certs'
    certutil='/mnt/c/Windows/System32/certutil.exe'
    ca_name='123CA'
    org_name='123CA'
    dept='DevOps'
    email='ali@teqnius.com'
    country='IN'
    state='Kerala'
    city='Calicut'
    pass='1234'

    #Create Cert Directory in Apache Folder
    sudo mkdir $cert_path_wsl

    #Generate Private Key for Generating our Own Root CA
    openssl genrsa -des3 -out $cert_path_wsl/$ca_name.key -passout pass:$pass 2048

    #Generate Root Certificate for our CA ()
    openssl req -x509 -new -nodes -key $cert_path_wsl/$ca_name.key -sha256 -days 1825 -out $cert_path_wsl/$ca_name.pem -passin pass:1234 \
    -subj "/C=$country/ST=$state/L=$city/O=$org_name/OU=$dept/CN=$org_name/emailAddress=$email"

    #Add Root Certificate to Windows Trust Store
    sudo mkdir $cert_path_win_wsl
    sudo cp $cert_path_wsl/$ca_name.pem $cert_path_win_wsl/$ca_name.pem
    $certutil -addstore -enterprise -f -v root "C:\local_dev_certs\\$ca_name.pem"

    #==================================================================#
    # Generate and Install SSL Certificate for Locahost)               #
    #==================================================================#

    #Generate Private Key
    openssl genrsa -out $cert_path_wsl/localhost.key 2048

    #Generate CSR
    openssl req -new -key $cert_path_wsl/localhost.key -out $cert_path_wsl/localhost.csr -passin pass:1234 \
    -subj "/C=$country/ST=$state/L=$city/O=$org_name/OU=$dept/CN=$org_name/emailAddress=$email"

    #Create SSL Configuration File
    touch $cert_path_wsl/sslconf.ext
    sudo echo -e "authorityKeyIdentifier=keyid,issuer
    basicConstraints=CA:FALSE
    keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
    subjectAltName = @alt_names

    [alt_names]
    DNS.1 = localhost" > "$cert_path_wsl/sslconf.ext"

    #Generate SSL Certificate using Root Certificate and Private Key
    openssl x509 -req -in $cert_path_wsl/localhost.csr -CA $cert_path_wsl/$ca_name.pem -CAkey $cert_path_wsl/$ca_name.key -CAcreateserial \
    -out $cert_path_wsl/localhost.crt -days 825 -sha256 -extfile $cert_path_wsl/sslconf.ext -passin pass:1234

    #Install Generated SSL Certificate on Apache Server
    #Backup Existing Default SSL Configuration
    cp /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf.bak
    sed -i 's/SSLCertificateFile.*/SSLCertificateFile \/etc\/apache2\/ssl_cert\/localhost.crt/g' /etc/apache2/sites-available/default-ssl.conf
    sed -i 's/SSLCertificateKeyFile.*/SSLCertificateKeyFile \/etc\/apache2\/ssl_cert\/localhost.key/g' /etc/apache2/sites-available/default-ssl.conf
    #Force HTTPS
    # sed -i 's/<\/VirtualHost>/Redirect "\/" "https:\/\/localhost\/"\
    # <\/VirtualHost>/g' /etc/apache2/sites-available/000-default.conf
    # sed -i 's/\<\/VirtualHost\>/1\<\/VirtualHost\>/g' /etc/apache2/sites-available/000-default.conf
    # sed -i 's/\<\/VirtualHost\>/ Redirect "/" "https://localhost/"\\n\<\/VirtualHost\>/g' /etc/apache2/sites-available/000-default.conf


    sudo ufw allow 'Apache Full'
    sudo ufw delete allow 'Apache'

    sudo a2enmod ssl
    sudo a2enmod headers
    sudo a2ensite default-ssl

    service apache2 restart
    xdg-open "https://localhost"




