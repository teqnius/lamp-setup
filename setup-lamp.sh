#!/bin/bash
echo -e "\n\n\e[96m==============================="
echo -e "\e[96m= Teqnius LAMP Install Script ="
echo -e "===============================\e[39m"


# 0. Variables
    db_password=123

# 1. Update Package List and Get Updates

    echo -e "\n\n\e[92m1. Updating Apt Packages and upgrading latest patches\n\e[39m"
    sudo apt-get update -y && sudo apt-get upgrade -y


# 2. Install Apache2 Web Server

    echo -e "\n\n\e[92m2. Installing Apache2 Web Server and Utilities\n\e[39m"
    sudo apt install -y apache2 ssl-cert openssl xdg-utils


# 3. Install PHP and Dependencies

    echo -e "\n\n\e[92m3. Installing PHP and Dependencies\n\e[39m"
    sudo apt install -y php php-mysql libapache2-mod-php php-common php-cli php-fpm php-curl php-pear php-mcrypt php-gd php-dev

#4. Install MySQL Server and Dependencies
    # https://bertvv.github.io/notes-to-self/2015/11/16/automating-mysql_secure_installation/
    # https://serversforhackers.com/c/installing-mysql-with-debconf
    # https://www.vic-l.com/automate-mysql-secure-installation-using-bash/

    echo -e "\n\n\e[92m4. Installing MySQL Server and Dependencies\n\e[39m"
    sudo apt-get -y install mysql-server

    sudo service mysql start

    sed -i 's/127\.0\.0\.1/0\.0\.0\.0/g' /etc/mysql/my.cnf
    sudo mysql -e 'USE mysql; UPDATE `user` SET `Host`="%" WHERE `User`="root" AND `Host`="localhost"; DELETE FROM `user` WHERE `Host` != "%" AND `User`="root"; FLUSH PRIVILEGES;'

    # MySQL Secure Installation - The following queries do all the stuff done by mysql_secure_installation script
    sudo mysql -e "ALTER USER root IDENTIFIED BY '$db_password';FLUSH PRIVILEGES;"
    sudo mysql -e "DELETE FROM mysql.user WHERE User='';"
    sudo mysql -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
    sudo mysql -e "DROP DATABASE test;DELETE FROM mysql.db WHERE Db='test' OR Db='test_%';"

    # Create a new MySQL user
    sudo mysql -u root -p$db_password -e "CREATE USER 'wp' IDENTIFIED BY '$db_password';GRANT ALL PRIVILEGES ON *.* TO 'wp';FLUSH PRIVILEGES;"

#5. Install PHPMyAdmin and Dependencies

    echo -e "\n\n\e[92m5. Installing PHPMyAdmin and Dependencies\n\e[39m"
    # Enable extensions for PHPMyAdmin
    sudo phpenmod mcrypt
    sudo phpenmod mbstring
    export DEBIAN_FRONTEND=noninteractive

    # Set root password for mysql
    debconf-set-selections <<< 'mysql-server mysql-server/root_password password $db_password'
    debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password $db_password'
    # Set phpmyadmin paramaters for install
    debconf-set-selections <<< 'phpmyadmin/debconfig-install boolean true'
    debconf-set-selections <<< 'phpmyadmin/mysql/admin-user string root'
    debconf-set-selections <<< 'phpmyadmin/mysql/admin-pass password $db_password'
    debconf-set-selections <<< 'phpmyadmin/mysql/app-pass password $db_password'
    debconf-set-selections <<< 'phpmyadmin/app-password-confirm password $db_password'
    debconf-set-selections <<< 'phpmyadmin/reconfigure-websever multiselect none'
    debconf-set-selections <<< 'phpmyadmin/database-type select mysql'
    debconf-set-selections <<< 'phpmyadmin/setup-password password $db_password'

    apt-get install -y phpmyadmin
    #Install PHPMyAdmin
    sudo apt install php-mbstring php-zip php-gd php-json php-curl -y
    sudo apt install phpmyadmin -y
    #Edit Apache Conf File to include phpmyadmin
    sudo echo "Include /etc/phpmyadmin/apache.conf" >> "/etc/apache2/apache2.conf"

#6. Install WP CLI and Wordmove

    echo -e "\n\n\e[92m6. Installing WP CLI\n\e[39m"
    curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
    php wp-cli.phar --info
    chmod +x wp-cli.phar
    sudo mv wp-cli.phar /usr/local/bin/wp
    wp --info

    #Wordmove Install
    sudo apt install ruby-full -y
    sudo gem install wordmove -v 4.0.1 #We choose this to solve wordmove issues

    #Install WP Install Script By Teqnius
    sudo echo 'alias wp-setup="wget --no-cache -O - https://gitlab.com/teqnius/wp-setup/-/raw/master/wp-setup.sh | sudo bash"' >> ~/.bashrc
    source ~/.bashrc

#7. Create Root CA and Add to Windows Trust Store

    echo -e "\n\n\e[92m7. Creating Root CA and Adding to Windows Trust Store\n\e[39m"
    #====================================================#
    # Create Root CA in WSL Apache Folder (/etc/apache2) #
    #====================================================#

    #https://www.computertechblog.com/import-a-certificate-to-trusted-root-certification-authorities-using-command-prompt/
    #https://deliciousbrains.com/ssl-certificate-authority-for-local-https-development/
    #https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-apache-in-ubuntu-18-04
    #https://www.dionysopoulos.me/forge-your-own-ssl-certificates-for-local-development.html

    cert_path_wsl='/etc/apache2/ssl_cert'
    cert_path_win='C:\local_dev_certs'
    cert_path_win_wsl='/mnt/c/local_dev_certs'
    certutil='/mnt/c/Windows/System32/certutil.exe'
    ca_name='teqnius' #no spaces allowed
    org_name='Teqnius Business Solutions'
    dept='DevOps'
    email='ali@teqnius.com'
    country='IN'
    state='Kerala'
    city='Calicut'
    pass='1234'

    #Create Cert Directory in Apache Folder
    sudo mkdir $cert_path_wsl

    #Generate Private Key for Generating our Own Root CA
    openssl genrsa -des3 -out $cert_path_wsl/$ca_name.key -passout pass:$pass 2048

    #Generate Root Certificate for our CA ()
    openssl req -x509 -new -nodes -key $cert_path_wsl/$ca_name.key -sha256 -days 1825 -out $cert_path_wsl/$ca_name.pem -passin pass:1234 \
    -subj "/C=$country/ST=$state/L=$city/O=$org_name/OU=$dept/CN=$org_name/emailAddress=$email"

    #Add Root Certificate to Windows Trust Store
    sudo mkdir $cert_path_win_wsl
    sudo cp $cert_path_wsl/$ca_name.pem $cert_path_win_wsl/$ca_name.pem
    $certutil -addstore -enterprise -f -v root "C:\local_dev_certs\\$ca_name.pem"

    #==================================================================#
    # Generate and Install SSL Certificate for Locahost)               #
    #==================================================================#

    #Generate Private Key
    openssl genrsa -out $cert_path_wsl/localhost.key 2048

    #Generate CSR
    openssl req -new -key $cert_path_wsl/localhost.key -out $cert_path_wsl/localhost.csr -passin pass:1234 \
    -subj "/C=$country/ST=$state/L=$city/O=$org_name/OU=$dept/CN=$org_name/emailAddress=$email"

    #Create SSL Configuration File
    touch $cert_path_wsl/sslconf.ext
    sudo echo -e "authorityKeyIdentifier=keyid,issuer
    basicConstraints=CA:FALSE
    keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
    subjectAltName = @alt_names

    [alt_names]
    DNS.1 = localhost" > "$cert_path_wsl/sslconf.ext"

    #Generate SSL Certificate using Root Certificate and Private Key
    openssl x509 -req -in $cert_path_wsl/localhost.csr -CA $cert_path_wsl/$ca_name.pem -CAkey $cert_path_wsl/$ca_name.key -CAcreateserial \
    -out $cert_path_wsl/localhost.crt -days 825 -sha256 -extfile $cert_path_wsl/sslconf.ext -passin pass:1234

    #Install Generated SSL Certificate on Apache Server
    #Backup Existing Default SSL Configuration
    cp /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf.bak
    sed -i 's/SSLCertificateFile.*/SSLCertificateFile \/etc\/apache2\/ssl_cert\/localhost.crt/g' /etc/apache2/sites-available/default-ssl.conf
    sed -i 's/SSLCertificateKeyFile.*/SSLCertificateKeyFile \/etc\/apache2\/ssl_cert\/localhost.key/g' /etc/apache2/sites-available/default-ssl.conf

    #Force HTTPS
    # sed -i 's/<\/VirtualHost>/Redirect "\/" "https:\/\/localhost\/"\
    # <\/VirtualHost>/g' /etc/apache2/sites-available/000-default.conf
    # sed -i 's/\<\/VirtualHost\>/1\<\/VirtualHost\>/g' /etc/apache2/sites-available/000-default.conf
    # sed -i 's/\<\/VirtualHost\>/ Redirect "/" "https://localhost/"\\n\<\/VirtualHost\>/g' /etc/apache2/sites-available/000-default.conf

    sudo a2enmod ssl
    sudo a2enmod headers
    sudo a2ensite default-ssl
    sudo a2enmod rewrite

#8  Adjust Permissions and Firewall

    echo -e "\n\n\e[92m8. Adjusting Permissions and Firewall WP CLI\n\e[39m"
    # Adjust Firewall
    sudo ufw allow in "Apache Full"
    sudo ufw enable
    sudo ufw allow 22
    sudo ufw allow 3306

    # Allow Read/Write for Owner
    sudo chmod -R 0755 /var/www/
    sudo chown -R $USER:$USER /var/www/

    # Create info.php for testing php processing
    sudo echo "<?php phpinfo(); ?>" > /var/www/html/info.php

    # Allow to run Apache and MySQL on boot up
    sudo systemctl enable apache2
    sudo systemctl enable mysql

    #Restart Apache and MySQL
    sudo service apache2 restart
    sudo service mysql restart

    # Open localhost in the default browser
    xdg-open "http://localhost"
    xdg-open "https://localhost/info.php"
    xdg-open "https://localhost/phpmyadmin"

# TODO
    #1 -  Change Webroot to /mnt/Projects/WSL/ - DONT DO - Keep Files in WSL Filesystem itself for far far better performance
    #2 - Make it completely non interactive - like a one click install