## Installation

### Automatic

Run the below command in terminal:

```bash
wget --no-cache -O - https://gitlab.com/teqnius/lamp-setup/-/raw/master/setup-lamp.sh | bash
```

[:arrow_right: Go to next steps](#next-steps)

### Manual

First off, run the below command to update package index:

```bash
sudo apt update
```

Then run these commands in terminal separately to install each component:

> Apache

```bash
sudo apt install apache2
```

> MySQL

```bash
sudo apt install mysql-server
```

> PHP

```bash
sudo apt install php php-mysql libapache2-mod-php php-cli
```

> Adjust Firewall

```bash
sudo ufw allow in "Apache Full"
```

> Adjust permissions

```bash
sudo chmod -R 0755 /var/www/html/
```

> Allow running Apache on boot up

```bash
sudo systemctl enable apache2
```

> Start Apache server

```bash
sudo service apache2 start
```

> Test Apache

```bash
xdg-open "http://localhost"
```

> Create sample PHP script file

```bash
sudo echo "<?php phpinfo(); ?>" > /var/www/html/info.php
```

> Run sample PHP script file

```bash
xdg-open "http://localhost/info.php"
```

[:arrow_right: Go to next steps](#next-steps)

## Next Steps

### Install phpMyAdmin

Run the below command in terminal to install phpMyAdmin and it's prerequisites:

```bash
sudo apt install phpmyadmin php-mbstring php-gettext
```

And then enable required extensions:

```bash
sudo phpenmod mcrypt
sudo phpenmod mbstring
```
Then restart Apache server:

```bash
sudo service apache2 restart
```

Now navigate to the phpMyAdmin:

```bash
xdg-open "http://localhost"
```
```
PHPMyAdmin Not Starting Error Fix: https://stackoverflow.com/a/26896884/5709578
```
---

All done. now you have Apache2, MySQL, PHP installed.

Forked from creation of :heart: by [Bayat](https://github.com/EmpireWorld)